"""Use AttributeBuilder to generate attribute resource file from yml specification"""

import argparse
import os

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate attribute resource from yml description')
    parser.add_argument('template_filepath', help='Attribute template filename/path (.sbt)')
    parser.add_argument('yml_filepath', help='YAML file specifying the attributes to generate (.yml | .yaml')
    parser.add_argument('-a', '--attribute_collection', nargs='+', help='definitions to write to attribute collection')
    parser.add_argument('-l', '--less_verbose', action='store_true', help='reduce output dumped to the console')
    parser.add_argument('-m', '--model_filepath', help='path to SMTK model resource (.smtk)')
    parser.add_argument('-o', '--output_filepath', default='attributes.smtk', help='output filename/path (attributes.smtk)')
    parser.add_argument('--read_sbt', action='store_true', help='use smtk attribute reader (for debug)')
    # parser.add_argument('-s', '--skip_instances', action='store_true', help='skip initializing instanced attributes')

    args = parser.parse_args()
    # print(args)

    # Import internal modules *after* argparse, in order to display usage/help without smtk
    import yaml
    from smtk_tools.attribute_builder import AttributeBuilder
    from smtk_tools.resource_io import ResourceIO

    # Load yml specification
    spec = None
    print('Loading yaml file:', args.yml_filepath)
    with open(args.yml_filepath) as fp:
        content = fp.read()
        spec = yaml.safe_load(content)
    assert spec is not None

    # Initialize ResourceIO and load resources
    model_resource = None
    res_io = ResourceIO()
    if args.model_filepath:
        print('Loading model resource file:', args.model_filepath)
        model_resource = res_io.read_resource(args.model_filepath)
        assert model_resource is not None, 'failed to load model resource from file {}'.format(args.model_filepath)

    # Load the attribute resource
    if args.read_sbt:
        att_resource = res_io.read_sbt_file(args.template_filepath)
    else:
        att_resource = res_io.import_resource(args.template_filepath)
    assert att_resource is not None, 'failed to import attribute template from {}'.format(args.template_filepath)

    # Associate the model resource
    if model_resource is not None:
        att_resource.associate(model_resource)

    # Initialize builder and populate the attributes
    verbose = not args.less_verbose
    builder = AttributeBuilder(verbose=verbose)
    builder.build_attributes(att_resource, spec, model_resource=model_resource)

    # Write the attribute resource to either .sbi or .smtk
    base, ext = os.path.splitext(args.output_filepath)
    if args.attribute_collection:
        output_path = 'attributes.sbi' if ext == '.smtk' else args.output_filepath
        res_io.write_attribute_collection(att_resource, args.attribute_collection, output_path)
    elif ext == '.sbi':
        res_io.write_sbi_file(att_resource, args.output_filepath)
    else:
        res_io.write_resource(att_resource, args.output_filepath)
    print('Wrote', args.output_filepath)
