<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="polynomial">
      <ItemDefinitions>
        <Double Name="coefficients" NumberOfRequiredValues="4">
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="material">
      <ItemDefinitions>
        <Double Name="density" />
        <Double Name="specific-heat">
          <ExpressionType>polynomial</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Group" Title="Example" TopLevel="true" TabPosition="North"
    FilterByAdvanceLevel="false" FilterByCategory="false">
      <Views>
        <View Title="Materials" />
        <View Title="Expressions" />
      </Views>
    </View>
    <View Type="Attribute" Title="Materials">
      <AttributeTypes>
        <Att Type="material" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Expressions">
      <AttributeTypes>
        <Att Type="polynomial" />
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
