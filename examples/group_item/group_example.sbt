<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="SubgroupExample">
      <ItemDefinitions>
        <Group Name="Coords" Extensible="true">
          <ItemDefinitions>
            <Double Name="X" NumberOfRequiredValues="1"></Double>
            <Double Name="Y" NumberOfRequiredValues="1"></Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Attribute" Title="Example" TopLevel="true"
    FilterByAdvanceLevel="false" FilterByCategory="false">
      <AttributeTypes>
        <Att Type="SubgroupExample"></Att>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
