# PyYAML Source Files

The files in this directory are copied from the PyYAML repository at https://github.com/yaml/pyyaml, and are governed by the `LICENSE` file in this directory. The specific files are taken from the pyyaml 5.3.1 tag (commit 538b5c93, 18-Mar-2020), subdirectory `lib3/yaml`.
