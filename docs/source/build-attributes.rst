Attribute Builder
==================
The main feature of SMTK Tools is a Python script ``build_attribures.py`` for generating attribute resources from YAML-formatted specifications. The Python script uses SMTK python libraries, so it must be run with a Python interpreter that can import the smtk modules. For example, the ``pvpython`` binary distributed with CMB modelbuilder is suitable.

Command Line
------------

The script usage is posted below. In brief, the script requires two inputs: (i) an attribute template file (.sbt) and (ii) a specification file (.yml) enumerating the attributes to be created and edited. In addition, an smtk model resource file (.smtk) can be included as an optional argument, so that model associations can be specified in the yml file. The file name/path for the attribute resource created by the script can be optionally specified; by default "attributes.smtk" is used.

.. code-block:: console

    usage: build_attributes.py [-h]
                            [-m MODEL_FILEPATH]
                            [-o OUTPUT_FILEPATH]
                            template_filepath yml_filepath

    Generate attribute resource from yml description

    positional arguments:
    template_filepath     Attribute template filename/path (.sbt)
    yml_filepath          YAML file specifying the attributes to generate

    optional arguments:
    -h, --help            show this help message and exit
    -m MODEL_FILEPATH, --model_filepath MODEL_FILEPATH
                            path to SMTK model resource (.smtk)
    -o OUTPUT_FILEPATH, --output_filepath OUTPUT_FILEPATH
                            output filename/path (attributes.smtk)


Processing Sequence
-------------------
The basic processing sequence is:

1. The YAML specification file is loaded as a python object (list).
2. If an SMTK model resource file (.smtk) was included in the command line, that file is loaded.
3. The SMTK template file (.sbt) is loaded as an SMTK attribute resource.
4. If an SMTK model resource was loaded in step 2, it is associated to the attribute resource.
5. The software iterates the views in the SMTK template file, creating attribute instances that are designated in any Analysis, Instanced, and Selector views. (Only views that are accessible in the tree descending from the top-level view are processed.)
6. The software iterates the members of the YAML specification file, creating or editing each attribute as specified.
7. The attribute resource is written to the filesystem.
