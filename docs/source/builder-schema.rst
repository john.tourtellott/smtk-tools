Specification Schema
====================
A specification object is used by the attribute builder code to create and edit attributes in an SMTK attribute resource. The schema for the specification object is described here using YAML format because that format is supported by the top-level ``build_attributes.py`` script. In practice, the specification can be a list or any other python iterable with members that are dictionary objects. Each list member specifies one action, to either create a new attribute or edit an existing attribute.

The main logic for creating and editing attributes is found in the ``AttributeBuilder`` class. Note that, before processing the specification, the ``AttributeBuilder`` first instantiates any SMTK attributes that are defined in the template Analysis, Instanced, and Selector views. These attributes can be edited, but additional instances should not be created in the specification.


Create Attribute
----------------
The full yml syntax to create one attribute is:

.. code-block:: yaml

    - action: create  # optional
      type: string    # required
      name: string    # optional

As noted in the example, only the ``type`` item is required. The ``action: create`` item is not required because it is the ``AttributeBuilder`` default action. The ``name`` item is not required because SMTK will assign an attribute name if one is not specified.


Edit Attribute
--------------
There are two ways to select an existing attribute for editing:

.. code-block:: yaml

    # Instanced attributes only
    - action: edit
      type: string

    # Any attribute
    - action: edit
      name: string

Note that the ``action: edit`` item is required for both cases. The first case, with the attribute type specified, is only valid when there is exactly one attribute of the specified type in the resource. This is generally the case for attributes defined in Analysis, Instanced, and Selector views. The second case can be used to specify any attribute in the resource by its name.


Attribute Items
---------------
Attribute items are specified by an ``items`` key. The ``items`` value is a list of item specifiers that can use the keywords ``name``, ``value``, ``enable``, ``items`` (for recursion of group items), and ``children`` (for recursion with discrete-value items). The ``name`` is required for each member in the ``items`` list.

.. code-block:: yaml

    - type: string
      items:
        - name: string  # required
          value: float | int | string | list
          enable: boolean
          items:       # for group items only
          children:    # for discrete items only

**Standard Format for Value Items**
For SMTK "value" items (double, int, string), the ``value`` can be specified as either a single value, or a list of values.

Examples:

.. code-block:: yaml

    # Add boundary condition with 2 properties (items)
    - type: boundary-condition
      items:
        - name: temperature
          value: 25.1
        - name: velocity
          value: [0.0, 0.0, -9.8]

    # Edit options attribute, setting nested start time
    # and optional end time
    - action: edit
      type: options
      items:
        - name: controller  # (group item)
          items:
            - name: start-time
              value: 0.0
            - name: end-time  # (optional)
              enable: true
              value: 42.0

**Format for Value Item Expressions**
SMTK value items can be assigned an "expression" instead of a constant value. An ``expression`` keyword specifies the name of the attribute representing the expression. The ``value`` and ``expression`` keywords should not be used for the same item, but if both are present, the ``expression`` will be used. The ``expression`` can be specified as a single string or a list of strings.

Example:

.. code-block:: yaml

    # Create attribute that represents a polynomial expression.
    - type: polynomial
      name: specific-heat-vs-temp
      items:
        - name: coefficients
          value: [2.406e+2, 2.50167, -1.1231e-3, 1.700e-7]

    # Create attribute with item that references the polynomial
    # expression (attribute).
    - type: material
      items:
        - name: density
          value: 1750
        - name: specific-heat
          expression: specific-heat-vs-temp

**Format for Component Items**
For SMTK component item values, which are attributes or model entities, a special notation is used. This notation is a list, which can be one of 3 formats:

* To set attribute references, the format is a list with the string "attribute" as its first element followed by the *names* of the attributes to assign to the component: ``value: [attribute, name1, name2, ...]``.
* To set model *face* entities, the format is a list with the string "face" as its first element followed by integers set to the "pedigree id" property of the model face entities: ``value: [face, id1, id2, ...]``.
* To set model *volume* entities, the format is a list with the string "volume" as its first element followed by integers set to the "pedigree id" property of the model volume entities: ``value: [face, id1, id2, ...]``.

Examples:

.. code-block:: yaml

    # Set fan inlet to a model face
    - action: edit
      type: fan
      items:
        - name: inlet
          value: [face, 21]

    # Create shell attribute with 2 layers
    - type: layer
      name: layer1
      items:
        - 'thickness': 0.1
        - 'conductivity': 0.102
    - type: layer
      name: layer2
      items:
        - 'thickness': 0.25
        - 'conductivity': 3.14
    - type: shell
      items:
        - name: layers
          value: [attribute, layer1, layer2]


**Shorthand Format for Value Items**
The ``AttributeBuilder`` also recognizes a shorthand notation for cases where only an item value is set. The shorthand is a single-item dictionary with the key set to the item name and the value set to the item value. Standard practice is to put the key in single quotes (though not *required* by the YAML standard) to indicate that the key is not an ``AttributeBuilder`` keyword. Using this shorthand, a previous example can be written as:

.. code-block:: yaml

    - type: boundary-condition
      items:
        - 'temperature': 25.1
        - 'velocity': [0.0, 0.0, -9.8]

    - action: edit
      type: options
      items:
        - name: controller
          items:
            - 'start-time': 0.0
            # The "edit-time" item does not use the shorthand notation
            # because we also set the enable flag.
            - name: end-time
              enable: true
              value: 42.0

**Format for Group Items with Multiple Subgroups**
To edit extensible groups, the keyword "subgroups" is used to designate a list of specifications, one for each subgroup. Because each subgroup specification is a list itself, the contents of the "subgroups" keyword is a nested list.

Example:

.. code-block:: yaml

    # This example creates an attribute type "SubgroupExample",
    # which contains one extensible group item "Coords".
    # Each subgroup contains an X and Y value.
    #
    # This example uses the "subgroups" keyword to create 3
    # subgroups. Each of the 3 subgroup specifications is a
    # list. Different formats are used for each subgroup,
    # for illustrative purposes.

    - type: SubgroupExample
      items:
      - name: Coords
        subgroups:
          # Subgroup 1 using standard format
          - - name: X
              value: 3.14159
            - name: Y
              value: 2.71828
          # Subgroup 2 using shorthand format
          - - X: 1.1
            - Y: 2.2
          # Subgroup 3 using shorthand format
          # and alternate yaml list format
          - [X: 42, Y: 73]


Attribute Association
---------------------
SMTK attribute associations are specified using the ``associate`` key, with the value a list of dictionaries. Each dictionary has a single key that is one of "attribute", "face", or "volume".

* For "attribute", the value is the name, or a list of names, of the attribute(s) to associate.
* For "face", the value is in integer, or a list of integers, with the pedigree id of each model face entity to be associated.
* For "volume", the value is in integer, or a list of integers, with the pedigree id of each model volume entity to be associated.

Examples:

.. code-block:: yaml

    - type: logical-surface
      associate:
        - attribute: [surface2, surface4]

    - type: boundary-condition
      associate:
        - face: [13, 11]
      items: ...

    - type: material
      associate:
        - volume: 1
      items: ...


The association format will likely be updated to use the same format as reference items described above.


Limitations
-----------
Validiation of the specification is limited. In many cases, unrecognized keywords are ignored.

There is no support for deleting or copying attributes.

There is no support for setting SMTK DateTimeItem values.
