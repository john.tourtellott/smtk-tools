.. SMTK Tools documentation master file, created by
   sphinx-quickstart on Wed May 27 20:05:04 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SMTK Tools
==========

SMTK Tools provides a few utilities for working with SMTK_ resources and files.

.. _SMTK: https://gitlab.kitware.com/cmb/smtk

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   build-attributes
   builder-schema
   classes

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
