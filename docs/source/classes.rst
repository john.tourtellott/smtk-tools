Classes
=======
The SMTK Tools classes provide the heavy lifting for the attribute building capability.


AttributeBuilder
----------------
The ``AttributeBuilder`` class generates SMTK attribute resource instances from two inputs: (i) an SMTK attribute *resource* containing attribute definitions, and (ii) an attribute *specification* consisting of a list of dictionary items, each specifying an action to create and/or edit individual attributes.

Before iterating the spec, attributes are created for view types that internally create attributes in the modelbuilder application: Analysis, Instanced, Selector.

The code will abort if any specified action is invalid or otherwise cannot be carried out. Minimal diagnostic information is written out when this occurs.

The specification schema is described in :doc:`builder-schema`.

More Todo


ResourceIO
----------
The ``ResourceIO`` class provides methods for reading and writing SMTK resource files, and also for importing SMTK python operations.

More Todo


TemplateChecker
---------------
The ``TemplateChecker`` class provides lint-style checking for smtk attribute templates. The initial implementation checks for *unreachable* views and definitions. Unreachable views are ones that are specified in the ``<Views>`` section but are not a descendant of the top-level view. As such, unreachable views will not be displayed in the smtk attribute editor or CMB modelbuilder. Similarly, unreachable definitions are ones included in the template ``Definitions`` section but are not referenced by any view that is reachable from the top-level view. As such, attributes of unreachable definition types cannot be created or edited using the smtk attribute editor or modelbuilder.

The ``template_checker.py`` file can be run as a python script. The usage is:

::

    $ .../pvpython smtk_tools/template_checker.py -h
    usage: template_checker.py [-h] template_filepath

    Check smtk template for unreachable elements

    positional arguments:
    template_filepath  Attribute template filename/path (.sbt)

    optional arguments:
    -h, --help         show this help message and exit
