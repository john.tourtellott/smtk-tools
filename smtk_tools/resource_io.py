import os

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.resource
import smtk.session.vtk


OPERATION_SUCCEEDED = int(smtk.operation.Operation.SUCCEEDED)  # 3

class ResourceIO:
    """A class for SMTK resource file import, read and write functions."""

    def __init__(self):
        """"""
        self.op_manager = None
        self.res_manager = None

        # Initialize smtk managers
        self.res_manager = smtk.resource.Manager.create()
        self.op_manager = smtk.operation.Manager.create()
        smtk.attribute.Registrar.registerTo(self.res_manager)
        smtk.attribute.Registrar.registerTo(self.op_manager)
        smtk.attribute.Registrar.registerTo(self.res_manager)
        smtk.attribute.Registrar.registerTo(self.op_manager)
        smtk.session.vtk.Registrar.registerTo(self.res_manager)
        smtk.session.vtk.Registrar.registerTo(self.op_manager)
        smtk.operation.Registrar.registerTo(self.op_manager)
        self.op_manager.registerResourceManager(self.res_manager)

    def import_python_operation(self, path):
        """Loads python operation."""
        import_op = self.op_manager.createOperation(
            'smtk::operation::ImportPythonOperation')
        assert import_op is not None, \
            'error creating smtk::operation::ImportPythonOperation'
        import_op.parameters().find('filename').setValue(path)
        import_result = import_op.operate()
        import_outcome = import_result.findInt('outcome').value(0)
        assert import_outcome == OPERATION_SUCCEEDED, \
            'error loading python operation, outcome {}'.format(import_outcome)

        op_unique_name = import_result.findString("unique_name").value()
        op = self.op_manager.createOperation(op_unique_name)
        return op

    def import_resource(self, path, operation_type=None):
        """Imports native file into new SMTK resource.

        Works with attribute templates and models that can be
        loaded by smtk.session.vtk
        """
        if operation_type is None:
            # Check file extension because default ImportResource doesn't
            # handle attribute templates reliably.
            # https://gitlab.kitware.com/cmb/smtk/-/issues/421
            filename, ext = os.path.splitext(path)
            if (ext == '.sbt'):
                # Use import operation specific to attribute resource
                operation_type = 'smtk::attribute::Import'
            else:
                # Otherwise use the general smtk import operation
                operation_type = 'smtk::operation::ImportResource'

        import_op = self.op_manager.createOperation(operation_type)
        assert import_op.parameters().find('filename').setValue(path), \
            'failed to set operation filename'
        result = import_op.operate()
        outcome = result.findInt('outcome').value()
        assert outcome == OPERATION_SUCCEEDED, 'operation failed, outcome {}'.format(outcome)
        resource = result.find('resource').value()
        assert resource is not None, 'failed to import resource from {}'.format(path)
        return resource

    def read_resource(self, path, operation_type='smtk::operation::ReadResource'):
        """Reads SMTK resource file (*.smtk).

        Works with attribute resources and model resources of type smtk.session.vtk
        """
        read_op = self.op_manager.createOperation(operation_type)
        read_op.parameters().find('filename').setValue(path)
        result = read_op.operate()
        outcome = result.findInt('outcome').value()
        assert outcome == OPERATION_SUCCEEDED, 'read operation failed for path {}'.format(path)
        resource = result.find('resource').value()
        assert resource is not None, 'failed to read resource from {}'.format(path)
        return resource

    def read_sbt_file(self, path):
        """Reads attribute template (.sbt) file using AttributeReader.

        This method is provided for test/debug use only. It uses the SMTK AttributeReader
        to load an attribute resource instead of smtk::operation::ReadResource.
        """
        reader = smtk.io.AttributeReader()
        logger = smtk.io.Logger.instance()
        resource = smtk.attribute.Resource.create()
        err = reader.read(resource, path, logger)
        assert err == False, 'failed to read attribute file {}'.format(path)
        return resource

    def write_attribute_collection(self, att_resource, att_types, path):
        """Writes "attribute collection" of specified types to .sbi file.

        An attribute collection contains attribute instances that can be
        loaded into an attribute resource. The resulting .sbi file can then
        be loaded into an attribute resource containing the **same**
        defitions from which the collection was saved. The typical use-case
        is to store a set of material attributes to reuse across multiple
        projects.

        Args:
            att_resource: smtk.attribute.Resource
            att_types: list[string], types of attributes to write
            path: (string) filesystem path
        """
        writer = smtk.io.AttributeWriter()
        logger = smtk.io.Logger.instance()
        defn_list = list()
        for att_type in att_types:
            defn = att_resource.findDefinition(att_type)
            assert defn is not None, 'failed to find defintion type \"{}\"'.format(att_type)
            defn_list.append(defn)
        print('CHECKPOINT', defn_list)
        writer.treatAsLibrary(defn_list)
        err = writer.write(att_resource, path, logger)
        assert err == False, 'AttributeWriter reported error'

    def write_resource(self, resource, path=None, operation_type='smtk::operation::WriteResource'):
        """"""
        write_op = self.op_manager.createOperation(operation_type)
        write_op.parameters().associate(resource)
        if path is not None:
            write_op.parameters().find('filename').setIsEnabled(True)
            write_op.parameters().find('filename').setValue(path)

        write_result = write_op.operate()
        outcome = write_result.findInt('outcome').value()
        assert outcome == OPERATION_SUCCEEDED, 'failed to write resource to {}'.format(path)

    def write_sbi_file(self, att_resource, path):
        """Writes attribute resource to xml-format (.sbi) file."""
        writer = smtk.io.AttributeWriter()
        logger = smtk.io.Logger.instance()
        err = writer.write(att_resource, path, logger)
        assert err == False, 'failed to write attribute resource to file {}'.format(path)
