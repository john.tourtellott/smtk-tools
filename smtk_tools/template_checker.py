"""This code requires SMTK builds that include commit e00cedaf (15-Jun-2020). """

import smtk
import smtk.attribute
import smtk.view


class TemplateChecker:
    """Checks smtk template files for unreachable elements.

    Unreachable views are specified in the template but are not a descendant of
    the top-level view. As such they will not be displayed in the smtk attribute editor.

    Unreachable definitions are definitions that are not references by any view that is
    reachable from the top-level view. As such, they cannot be created or edited using the
    smtk attribute editor.
    """
    def __init__(self):
        self.defn_set = set()  # definition types (string)
        self.view_set = set()  # view titles (string)

    def check_definitions(self, att_resource):
        """"""
        self.defn_set.clear()
        defn_list = att_resource.definitions()
        type_list = [defn.type() for defn in defn_list]
        self.defn_set = set(type_list)
        print('Checking {} definitions'.format(len(self.defn_set)))

        top_view = att_resource.findTopLevelView()
        if top_view is not None:
            self._check_definitions_recursive(att_resource, top_view.details())
            self._check_attributes(att_resource)

        return list(self.defn_set)

    def check_views(self, att_resource):
        """Traverses view hierachy and identifies unreachable views.

        Returns a list of unreached view names.
        """
        self.view_set.clear()
        view_dict = att_resource.views()
        self.view_set = set(view_dict.keys())
        print('Checking {} views'.format(len(self.view_set)))

        top_view = att_resource.findTopLevelView()
        if top_view is not None:
            self.view_set.discard(top_view.name())
            self._check_view_recursive(att_resource, top_view.details())

        return list(self.view_set)

    def _check_attributes(self, att_resource):
        """Removes types of any attributes included in the template."""
        att_list = att_resource.attributes()
        for att in att_list:
            att_type = att.type()
            self.defn_set.discard(att_type)
            defn = att_resource.findDefinition(att_type)
            self._check_basetypes(defn)

    def _check_basetypes(self, defn):
        """Removes any base types of a definition."""
        base_defn = defn.baseDefinition()
        while base_defn is not None:
            self.defn_set.discard(base_defn.type())
            base_defn = base_defn.baseDefinition()

    def _check_definitions_recursive(self, att_resource, comp):
        """"""
        if comp.name() == 'View':
            title = comp.attributes().get('Title')
            if title is None:
                return  # can ignore item views
            view = att_resource.findView(title)
            if view is None:
                raise RuntimeError('View {} not found'.format(title))

            view_type = view.type()
            if view_type in ['Attribute', 'Instanced']:
                self._check_view_atts(att_resource, view)
            elif view_type == 'Group':
                self._check_definitions_recursive(att_resource, view.details())
            elif view_type in ['Analysis', 'Associations', 'Selector']:
                # These views do not require any definitions in the template
                return
            else:
                # Custom views might require definitions
                self._check_view_atts(att_resource, view)
            return

        # (else) process component children
        for i in range(comp.numberOfChildren()):
            child = comp.child(i)
            self._check_definitions_recursive(att_resource, child)

    def _check_view_atts(self, att_resource, view):
        """Checks view for attribute definitions.

        View details format is:
            <AttributeTypes> or <InstancedAttributes>
                <Att Type="">
        """
        comp = view.details()
        atts_comp = comp.child(0)

        if atts_comp is None or atts_comp.name() not in ['AttributeTypes', 'InstancedAttributes']:
            return

        for i in range(atts_comp.numberOfChildren()):
            att_comp = atts_comp.child(i)
            defn_type = att_comp.attributes().get('Type')
            self.defn_set.discard(defn_type)

            defn = att_resource.findDefinition(defn_type)
            if defn is None:
                raise RuntimeError('Resource does not have definition type {}'.format(defn_type))

            # Discard all base types
            self._check_basetypes(defn)

            # For attribute views, also discard subtypes
            if view.type() == 'Attribute':
                derived_list = att_resource.findAllDerivedDefinitions(defn, False)
                for derived_defn in derived_list:
                    derived_type = derived_defn.type()
                    self.defn_set.discard(derived_type)

    def _check_view_recursive(self, att_resource, comp):
        """"""
        if comp.name() == 'View':
            title = comp.attributes().get('Title')
            if title is None:
                return  # (item view)
            self.view_set.discard(title)
            view = att_resource.findView(title)
            if view is None:
                raise RuntimeError('View {} not found'.format(title))
            self._check_view_recursive(att_resource, view.details())
            return

        # (else) process component children
        for i in range(comp.numberOfChildren()):
            child = comp.child(i)
            self._check_view_recursive(att_resource, child)


if __name__ == '__main__':
    import argparse
    import resource_io

    parser = argparse.ArgumentParser(description='Check smtk template for unreachable elements')
    parser.add_argument('template_filepath', help='Attribute template filename/path (.sbt)')
    args = parser.parse_args()

    res_io = resource_io.ResourceIO()
    att_resource = res_io.read_sbt_file(args.template_filepath)

    checker = TemplateChecker()
    unreached_views = checker.check_views(att_resource)
    print('Unreached views ({}): {}'.format(len( unreached_views),  unreached_views))

    unreached_defns = checker.check_definitions(att_resource)
    print('Unreached definitions ({}): {}'.format(len(unreached_defns), unreached_defns))
